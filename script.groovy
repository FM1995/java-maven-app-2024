def buildJar() {
    echo "building the application..."
    sh 'mvn package'
    
} 

def buildImage() {
    echo "Building the docker image.."
    withCredentials([usernamePassword(credentialsId: 'Docker credentials' , passwordVariable: 'PASS', usernameVariable:'USER')]) {
        sh 'docker build -t fuad95/integration-test:2024 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push fuad95/integration-test:2024'
   }
}

def deployApp() {
    echo "Deploying the application..."
}




return this
